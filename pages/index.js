import Link from "next/link";
export default function Home({ posts }) {
  return (
    <>
      <h2 className="py-4 text-5xl font-bold text-center">Posts</h2>
      <div className="max-w-3xl p-4 mx-auto space-y-4">
        {posts.map((post) => (
          <Link href={`/post/${post.id}`} key={post.id}>
            <a className="relative flex items-center p-5 bg-gray-800 rounded-lg ring-green-400 hover:ring-2 group">
              <h2 className="absolute flex items-center justify-center w-10 h-10 bg-gray-900 rounded-full group-hover:ring-2 ring-green-400">
                {post.id}
              </h2>
              <h4 className="pl-14">{post.title}</h4>
            </a>
          </Link>
        ))}
      </div>
    </>
  );
}

export async function getStaticProps() {
  const res = await fetch(
    "https://jsonplaceholder.typicode.com/posts?_limit=20"
  );
  const posts = await res.json();

  return {
    props: {
      posts,
    },
  };
}
