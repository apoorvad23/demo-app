import Link from "next/link";
function Post({ post }) {
  return (
    <>
      <Link href="/">
        <a className="fixed px-4 py-2 font-bold text-gray-900 bg-green-400 rounded-lg left-6 bottom-6">
          Back
        </a>
      </Link>

      <h1 className="py-4 text-5xl font-bold text-center">Information</h1>

      <div
        className="max-w-sm p-4 mx-auto mt-10 space-y-4 font-bold rounded-lg ring-1 ring-green-400"
        key={post.id}
      >
        <h2 className="flex items-center justify-center w-10 h-10 font-bold text-gray-900 bg-green-400 rounded-full">
          {post.id}
        </h2>
        <p>{post.body}</p>
      </div>
    </>
  );
}

export async function getStaticPaths() {
  const res = await fetch(
    "https://jsonplaceholder.typicode.com/posts?_limit=20"
  );
  const posts = await res.json();
  const paths = posts.map((post) => "/post/" + post.id);
  return { paths, fallback: false };
}

export async function getStaticProps({ params }) {
  const res = await fetch(
    `https://jsonplaceholder.typicode.com/posts/${params.id}`
  );
  const post = await res.json();

  return { props: { post } };
}

export default Post;
